# Ce petit projet est fait avec [Create React App](https://github.com/facebook/create-react-app).

# Foncionnalités:

### Design responsive

### Animation des button onHover

### Filtrage par pays

### Creation des slider

### importation des fonts

### Header et footer sticky

### Utilisation d'un preprocesseur sass

### Gulp pour conversion et redimensionnement

### `J'ai pas eu l'icone pour le dropdown du filtrage`

### `J'ai fait quelques modification au fichier json pour faciliter le devloppement et ajouter des données manquantes`

# Available Scripts

### Dans le répertoire de projet, vous pouvez exécuter :

### `npm install`

l’installation de dépendances depuis la plateforme npm .

### `npm start`

Exécute l'application en mode développement. et execute les jobs gulp de conversion et redimensionnement <br>
Ouvrez [http://localhost:3000](http://localhost:3000) pour la voir dans le navigateur.

La page se rechargera si vous effectuez des modifications. <br>
Vous verrez également toutes les erreurs de la lint dans la console.
