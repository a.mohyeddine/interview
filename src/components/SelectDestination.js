import "../styles.css";
import React from "react";
import "../index.css";

const SelectDestination = ({ countries, search, updateSearch }) => (
  <div className="select">
    <select value={search} onChange={updateSearch}>
      <option value="">DESTINATIONS</option>
      {countries.map(country => (
        <option key={country} value={country}>
          {country}
        </option>
      ))}
    </select>
  </div>
);

export default SelectDestination;
