import "../styles.css";
import React from "react";
import "../index.css";

const Footer = () => (
  <footer className="footer">
    <p>
      REJOIGNEZ <b>EMIRATES | THE LIST</b>
    </p>
    <p>VOL + HÔTEL NÉGOCIÉS JUSQU'À -70%</p>
    <hr className="footer-separation" />
  </footer>
);

export default Footer;
