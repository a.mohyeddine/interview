import React, { useState, useEffect } from "react";
import Rating from "./Rating";
/* import "./App.css"; */
const DestinationCard = props => {
  const [slider, setSlider] = useState(false);
  const [currentImage, setCurrentImage] = useState(0);

  useEffect(() => {
    if (typeof props.image === "object") {
      setSlider(true);
    }
  }, []);

  const handlerPrev = () => {
    console.log("prev");
    if (currentImage !== 0) setCurrentImage(currentImage - 1);
    console.log(currentImage);
  };

  const handlerNext = () => {
    console.log("next");

    if (props.image.length !== currentImage + 1)
      setCurrentImage(currentImage + 1);
    console.log(currentImage);
  };

  return (
    <div className="card">
      {!slider ? (
        <img className="card-image" src={props.image} />
      ) : (
        <div style={{ position: "relative" }}>
          <img className="card-image" src={props.image[currentImage]} />
          <button
            className="carousel_control carousel_control__prev"
            onClick={() => handlerPrev()}
          >
            <span />
          </button>
          <button
            className="carousel_control carousel_control__next"
            onClick={() => handlerNext()}
          >
            <span />
          </button>
        </div>
      )}

      <p className="position-abs">{props.upto}</p>

      <h1>
        {props.country}
        <span className="span-1"> - {props.place}</span>
      </h1>

      <div>
        <h1 style={{ display: "inline", marginRight: "10px" }}>
          <span className="span-1">{props.label}</span>
        </h1>
        <Rating rating={props.rating} />
        <button className="button-link">></button>
      </div>
      <p>
        <span className="span-left">{props.tags[0].label}</span>
        <span className="span-right">{props.tags[1].label}</span>
      </p>
    </div>
  );
};

export default DestinationCard;
