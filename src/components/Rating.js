import React, { useState } from "react";
/* import "./App.css"; */

const Rating = props => {
  const prop = props.rating;
  return (
    <div className="review-rating">
      <div className="star">
        {Array.apply(null, { length: prop }).map((e, i) => (
          <img
            key={i}
            className={"star-img"}
            src="images/fullstar.png"
            alt=""
          />
        ))}
      </div>
    </div>
  );
};

export default Rating;
