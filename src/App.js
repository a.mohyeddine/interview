import React, { useEffect, useState } from "react";
import DestinationCard from "./components/DestinationCard";
import SelectDestination from "./components/SelectDestination";
import Footer from "./components/Footer";
/* import "./App.css"; */
const json = require("./destinations.json");

function App() {
  const [destinations, setDestination] = useState([]);
  const [search, setSearch] = useState("");

  let selectedDestination = selected =>
    selected !== "" ? json.filter(dest => dest.country === selected) : json;

  const countries = json
    .map(item => item.country)
    .filter((value, index, self) => self.indexOf(value) === index);

  const getSearch = e => {
    e.preventDefault();
  };

  useEffect(() => {
    setDestination(json);
  }, []);

  const updateSearch = e => {
    const input = e.target.value;
    setSearch(input);
    setDestination(selectedDestination(input));
  };

  return (
    <div className="App">
      <div className="header">
        <div className="header-text">
          <h1 className="_title">Découvrez les offres du moment</h1>
          <h2 className="_sub-title">VOL + HÔTEL JUSQU'À -70%</h2>
          <hr></hr>
        </div>
        <form className="search-form" onSubmit={getSearch}>
          <SelectDestination
            search={search}
            updateSearch={updateSearch}
            countries={countries}
          />
        </form>
      </div>
      <div className="card-grid">
        {destinations.map((destination, i) => (
          <DestinationCard
            key={`${destination.country} ${i}`}
            rating={destination.rating}
            place={destination.place}
            link={destination.link}
            image={destination.image}
            upto={destination.upto}
            country={destination.country}
            tags={destination.tags}
            label={destination.label}
          />
        ))}
      </div>
      <p className="text-footer" style={{ textAlign: "center" }}>
        COMMENT BENEFICIER DE L'OFFRE ?
      </p>
      <p
        className="sub-title-footer"
        style={{ fontWeight: 500, textAlign: "center" }}
      >
        150 € DE RÉDUCTION* DES 1000 € D'ACHAT
      </p>
      <hr></hr>
      <div className="code-promo">
        <p className="t1">Déjà membre ?</p>
        <p className="des">
          Votre code promo vous attend directement sur le site{" "}
          <a href="">en cliquant ici</a>
        </p>
      </div>

      <div className="steps-wrapper">
        <div className="stretch">
          <p className="first-letter height">1 REJOIGNEZ</p>
          <p className="red">EMIRATES | THE LIST</p>
        </div>

        <div className="stretch">
          <p className="first-letter height-center">2 RECEVEZ PAR MAIL</p>
          <p className="red">VOTRE BON DE 150 € OFFERT</p>
          <p className="text-bottom">UTILISER DÉS 1000 € D'ACHAT</p>
        </div>
        <div className="stretch">
          <p className="first-letter height">3 RÉSÉRVER VOTRE SÉJOUR </p>
          <p className="red">AVANT LE 19 SEPTEMBRE 2019 </p>
        </div>
      </div>
      <button className="inscription">JE M'INSCRIS</button>

      <Footer />
    </div>
  );
}

export default App;
