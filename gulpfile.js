var gulp = require("gulp");
var rename = require("gulp-rename");
var imagesConvert = require("gulp-images-convert");
var imageResize = require("gulp-image-resize");

gulp.task("convert", function() {
  return gulp
    .src("public/images/*.jpg")
    .pipe(imagesConvert({ targetType: "png" }))
    .pipe(rename({ extname: ".png" }))
    .pipe(gulp.dest("public/images/converted"));
});

gulp.task("resize", done => {
  gulp
    .src("public/images/converted")
    .pipe(
      imageResize({
        width: 369,
        height: 193,
        crop: true,
        upscale: false
      })
    )
    .pipe(gulp.dest("public/images/"));
  done();
});
